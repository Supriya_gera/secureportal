import { Component, OnInit, ViewChild} from '@angular/core';




@Component({
  selector: 'app-daysheet',
  templateUrl: './daysheet.component.html',
  styleUrls: ['./daysheet.component.css'],
  
})
export class DaysheetComponent implements OnInit {

  @ViewChild('calendar')
  calendar: any;
  value: Date;
  constructor() { }

  ngOnInit() {
  }

  openCalendar(event: any) {
    console.log('any');
    this.calendar.showOverlay(this.calendar.inputfieldViewChild.nativeElement);
    event.stopPropagation();
  }
  
}

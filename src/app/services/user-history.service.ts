import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class UserHistoryService {
  user:any;
  users= [{
    id: 1,
    name:'Raboud, Robert',
    address: '785 Sentry Ridge Xing Suwanee, GA 30024'
  
  },{
    id:2,
    name:'Balsamo, Gary',
    address: '10123 Rivendell Ln Charlette, NC 28269'
  },
  {
    id:3,
    name:'Balsamo, Gary',
    address: '10123 Rivendell Ln Charlette, NC 28269'
  },
  {
    id:4,
    name:'Raboud, Robert',
    address: '785 Sentry Ridge Xing Suwanee, GA 30024'
  },
  {
    id:5,
    name:'Balsamo, Gary',
    address: '10123 Rivendell Ln Charlette, NC 28269'
  },
  {
    id:6,
    name:'Raboud, Robert',
    address: '785 Sentry Ridge Xing Suwanee, GA 30024'
  
  },
  {
    id:7,
    name:'Balsamo, Gary',
    address: '10123 Rivendell Ln Charlette, NC 28269'
  },
  {
    id:8,
    name:'Raboud, Robert',
    address: '785 Sentry Ridge Xing Suwanee, GA 30024'
  
  },
  {
    id:9,
    name:'Balsamo, Gary',
    address: '10123 Rivendell Ln Charlette, NC 28269'
  },
  {
    id:10,
    name:'Balsamo, Gary',
    address: '10123 Rivendell Ln Charlette, NC 28269'
  },
  {
    id:11,
    name:'Balsamo, Gary',
    address: '10123 Rivendell Ln Charlette, NC 28269'
  },
  {
    id:12,
    name:'Balsamo, Gary',
    address: '10123 Rivendell Ln Charlette, NC 28269'
  },
  {
    id:13,
    name:'Balsamo, Gary',
    address: '10123 Rivendell Ln Charlette, NC 28269'
  },
  {
    id:14,
    name:'Balsamo, Gary',
    address: '10123 Rivendell Ln Charlette, NC 28269'
  },
  {
    id:15,
    name:'Balsamo, Gary',
    address: '10123 Rivendell Ln Charlette, NC 28269'
  }]
  
  constructor(private messageService: MessageService) { }
  getHistory(id: number): Observable<any> {
    // TODO: send the message _after_ fetching the hero
    return of(this.users.find(user => user.id === id));
  }
  
  getAllPatients() {
    return this.users;
  }
  
}

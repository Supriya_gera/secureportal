import { User} from '../model/user';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthService{
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    private isAuthenticated = false;
    private user: User;

    constructor(private router: Router, private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }
    
    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }
    
    loginDetail(authData: User){
        
        /*this.user = {
            username: authData.username,
            userId: Math.round(Math.random() * 10000).toString()
        }*/
        this.user = null;
        console.log(this.user);
        localStorage.setItem('currentUser', JSON.stringify(this.user));
        console.log('Local Storage data saved');
        //this.user = user;
        this.authSuccessfully();
         
    }
    
    register(user: User) {
        console.log(user);
        this.http.post<any>(`/users/register`, user)
        .pipe(map(user => {
            // login successful if there's a jwt token in the response
            console.log('I am here');
                console.log(user);
        }));
        console.log('Testing');
    }

    login(username: string, password: string) {
        return this.http.post<any>(`/users/authenticate`, { username, password })
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                console.log('I am here now');
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }

                return user;
            }));
    }

    isAuth(){
        return this.isAuthenticated;
    }
    private authSuccessfully(){
        
        this.isAuthenticated  = true;
        //this.authChange.next(true);
        this.router.navigate(['../../dashboard']);
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
        this.router.navigate(['/auth/login']);
    }

    getUser() {
        return {...this.user};
    }


}
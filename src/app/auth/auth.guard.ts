import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { take } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate, CanLoad{

    constructor(private router: Router, private authservice: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    
        const currentUser = this.authservice.currentUserValue;  
        if (currentUser){
            return true;
        }
        else{
            this.router.navigate(['/auth/login']);
        }

    }
    canLoad(route: Route){
        const currentUser = this.authservice.currentUserValue;  
        if (currentUser){
            return true;
        }
        else{
            this.router.navigate(['/auth/login']);
        }
    }
    
}

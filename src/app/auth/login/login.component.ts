import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User} from '../../model/user';
import {  AuthService } from '../auth.service';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  returnUrl: string;

  constructor(private formBuilder: FormBuilder, 
              private authservice: AuthService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      code: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  
  }
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
  }
  login(){

    /*this.authservice.loginDetail({
      username: this.registerForm.value.username
    });*/

    this.authservice.login(this.f.username.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                  console.log('Reached here');
                  this.router.navigate(['/dashboard']);
                },
                error => {
                    //this.alertService.error(error);
                });
    
  }
   
}


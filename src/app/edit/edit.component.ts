import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormArray,  Validators } from '@angular/forms';
import { Patients, Address, Demographics, Insurance, AddNote } from '../model/patients';




@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  searchValue:string = '';
  addEditPatientForm: FormGroup;
  
  constructor(private fb: FormBuilder, private router:Router) { }

  ngOnInit() {

    this.addEditPatientForm = this.fb.group({
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      company: ['', Validators.required],
      address: this.fb.array([this.fb.group({address:''})]),
      demographics: this.fb.array([this.fb.group({demographics:''})]),
      insurance: this.fb.array([this.fb.group({insurance:''})]),
      addnote: this.fb.array([this.fb.group({addnote:''})])
    });

  }

  

  /*getHistory(){
    this.router.navigate(['./patients']);
  }*/
  get address() {
    return this.addEditPatientForm.get('address') as FormArray;
  }
  get demographics() {
    return this.addEditPatientForm.get('demographics') as FormArray;
  }
  get insurance() {
    return this.addEditPatientForm.get('insurance') as FormArray;
  }
  get addnote() {
    return this.addEditPatientForm.get('addnote') as FormArray;
  }
  addaddress() {
    this.address.push(this.fb.group({address:''}));
  }

  deleteaddress(index) {
    this.address.removeAt(index);
    console.log(index);
  }
  adddemographics() {
    this.demographics.push(this.fb.group({demographics:''}));
  }

  deletedemographics(index) {
    this.demographics.removeAt(index);
  }
  addinsurance() {
    this.insurance.push(this.fb.group({insurance:''}));
  }

  deleteinsurance(index) {
    this.insurance.removeAt(index);
  }
  addaddnote() {
    this.addnote.push(this.fb.group({addnote:''}));
  }

  deleteaddnote(index) {
    this.addnote.removeAt(index);
  }
  
  
  

}

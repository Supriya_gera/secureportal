import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
/*import {AccordionModule} from "ngx-accordion";*/

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
    //accordion and accordion tab
import {MenuItem} from 'primeng/api'; 
import {AccordionModule} from 'primeng/accordion';   
import {CalendarModule} from 'primeng/calendar';
  


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './auth/login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthService } from './auth/auth.service';
import { ProfileComponent } from './profile/profile.component';
import { PatientsComponent } from './patients/patients.component';
import { FilterPipe } from './filterpipe';
import { UserHistoryComponent } from './user-history/user-history.component';
import { UserHistoryService } from './services/user-history.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthGuard } from './auth/auth.guard';
import { EditComponent } from './edit/edit.component';
import { JwtInterceptor, ErrorInterceptor, fakeBackendProvider } from './_helpers';
import { FooterComponent } from './footer/footer.component';
import { DaysheetComponent } from './daysheet/daysheet.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    DashboardComponent,
    ProfileComponent,
    PatientsComponent,
    FilterPipe,
   UserHistoryComponent,
   EditComponent,
   FooterComponent,
   DaysheetComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    NgxPaginationModule,
    HttpClientModule,
    AccordionModule,
   BrowserAnimationsModule,
    CalendarModule
    
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    fakeBackendProvider, AuthService, UserHistoryService, AuthGuard
  ],
  bootstrap: [AppComponent]
  
})
export class AppModule { }

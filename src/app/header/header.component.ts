import { Component, OnInit } from '@angular/core';
import {  AuthService } from '../auth/auth.service';
import { User } from '../model/user';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user: User;
  isAuth = false;
  currentUser: User;
  currentUserSubscription: Subscription;

  constructor(private authservice: AuthService) { 
    this.currentUserSubscription = this.authservice.currentUser.subscribe(user => {
      this.currentUser = user;
    });
    console.log(this.currentUser);
  }

  ngOnInit() {
    /*this.authservice.authChange.subscribe(
      authstatus => {
        console.log('Testong');
        console.log(authstatus);
        this.isAuth = authstatus;
    });*/

      this.user = this.authservice.getUser();
      console.log('Test');
      console.log(this.user);
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.currentUserSubscription.unsubscribe();
  }

  logout() {
    this.authservice.logout();
  }


}

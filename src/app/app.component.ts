import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { UserService } from './_services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'SecurePortal';
  constructor(private userService: UserService){}
  ngOnInit(){
    console.log('Initialized');
    //this.authService.initAuthListener();
    this.userService.register({firstName: "Gary", lastName: "Balsamo", 
        username: "gary", password: "password12345", id: 1, token: 'fake-jwt-token'})
    .pipe(first())
    .subscribe(
        data => {
            //this.alertService.success('Registration successful', true);
            //this.router.navigate(['/login']);
        },
        error => {
            //this.alertService.error(error);
            //this.loading = false;
        });
  }


}

import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { UserHistoryService }  from '../services/user-history.service';



@Component({
  selector: 'app-user-history',
  templateUrl: './user-history.component.html',
  styleUrls: ['./user-history.component.css']
})
export class UserHistoryComponent implements OnInit {
  user: any;
 
  private sub: any;
  
  constructor(private route: ActivatedRoute,
    private router:Router,
    private userservice: UserHistoryService,
    private location: Location
    
    ) { }

  ngOnInit() {
    this.getHistory();
  }
  getHistory(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.userservice.getHistory(id)
      .subscribe(user => this.user = user);
      console.log(this.user);
  }
  goBack(): void {
    this.location.back();
  }
   
  }
    
  

  


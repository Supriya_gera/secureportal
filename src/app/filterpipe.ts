import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'filterAll'
})
export class FilterPipe implements PipeTransform {

    transform(value: any, args?: any): any {
      if (!args) {
        return value;
      }
      return value.filter((val) => {
        let rVal = (val.name.toLocaleLowerCase().includes(args.toLocaleLowerCase())) || (val.address.toLocaleLowerCase().includes(args.toLocaleLowerCase()));
        return rVal;
      })
  
    }
  
  }
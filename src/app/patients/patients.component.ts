import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { UserHistoryService } from '../services/user-history.service';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.css']
})
export class PatientsComponent implements OnInit {
  aa:boolean=false;
  config: any;
  name="";
  selectedUser:any;
  id:number;
  users;
  
  constructor(private route: ActivatedRoute,  private router:Router, private userservice: UserHistoryService) { }

  ngOnInit() {
     this.users = this.userservice.getAllPatients();
     this.config = {
      itemsPerPage: 9,
      currentPage: 1,
      totalItems: this.users.length
    };

    console.log(this.config);
  }
  setIndex(ii){
    this.aa=ii;
    console.log
  }
  onSelect(id): void{
    this.router.navigate(['/history', id]);
  }
  pageChanged(event){
    this.config.currentPage = event;
  }

  goBack(): void {
    this.router.navigate(['/dashboard']);
  }
  edit(){
    this.router.navigate(['/edit']);
  }
  
}

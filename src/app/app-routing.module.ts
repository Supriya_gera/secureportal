import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { PatientsComponent } from './patients/patients.component';
import { UserHistoryComponent } from './user-history/user-history.component';
import { AuthGuard } from './auth/auth.guard';
import { EditComponent } from './edit/edit.component';
import { DaysheetComponent } from './daysheet/daysheet.component';



const routes: Routes = [
  {path: '', component: LoginComponent },
  {path: 'auth/login', component: LoginComponent },
  {path: 'dashboard', component: DashboardComponent , canActivate: [AuthGuard]},
  {path: 'profile', component: ProfileComponent },
  {path: 'patients', component: PatientsComponent , canActivate: [AuthGuard]},
  {path: 'history/:id', component: UserHistoryComponent , canActivate: [AuthGuard]},
  {path: 'edit', component: EditComponent, canActivate: [AuthGuard]},
  {path: 'daysheet', component: DaysheetComponent}
  
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

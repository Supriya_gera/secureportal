export class Patients {
    fname: string;
    lname: string;
    company: string;
    address: Address[];
    demographics: Demographics[];
    insurance: Insurance[];
    addnote: AddNote[]
}

export class Address {
    address: string
}
export class Demographics {
    demographics: string
}
export class Insurance {
    insurance: string
}
export class AddNote {
    addnote: string
}